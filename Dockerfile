FROM nginx:alpine
LABEL author "Soriyath Straessle<soriyath@leading.works>"

WORKDIR /usr/src/app
COPY --chown=nginx:nginx nginx.conf nginx-headers.conf /etc/nginx/
COPY --chown=nginx:nginx ./public .
EXPOSE 80
STOPSIGNAL SIGTERM
ENTRYPOINT [ "nginx", "-g", "daemon off;" ]
