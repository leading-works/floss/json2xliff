# json2xliff

> a utility tool to convert json translation files into xliff translation
> files, and back

## Usage

### xliff2json

The tool uses the `srcLang` and `trgLang` attributes of the xliff 2.0 file to
assess how to transpile into json flat translation file. Please make sure the
arguments to the tool match the xliff file.
See also the XSD specification: ./specifications/xliff_core_2.0.xsd .
See usage with: `yarn json2xliff -h`.

The json file will be created in the current working directory with the
provided tag and target language. If the file already exists, the tool throws
an error.

The tool automatically detects if the XLIFF file has version 2.0, and uses the
correct mapper to convert the texts. If it is not 2.0, it assumes the XLIFF
file is compliant with version 1.2 and uses another adapter. Since XLIFF files
contains the version of the schema used, using the `--xliff1` flag is not
necessary.

With the `--stdout` / `-o` flag, you can output to `stdout` and pipe into a
tool like `jq` to format the json string. A further pipe into `pbcopy` puts the
text into the clipboard, ready to be pasted where you need it.
e.g. `yarn json2xliff .... --stdout | jq . | pbcopy`.

```bash
yarn json2xliff --srcLang "de-CH" \
                --trgLang "fr-CH" \
                --tag "v1.5.1" \
                --xliff fixtures/uneven_example_v1.5.0_de-CH.xlf \
                --stdout

yarn json2xliff --srcLang "fr-CH" \
                --trgLang "de-CH" \
                --tag "v1.5.1" \
                --xliff fixtures/uneven_example_v1.5.0_fr-CH.xlf \
                --stdout
```

The output can be sorted and beautified with the flags `--sort` and `--beautify`.

### json2xliff

XLIFF translation files are XML bitext file containing both the source language
texts, as well as the target language texts if they exists (in case of a
translation reviews, or completion of new translations, for example).
See [the wikipedia page](https://en.wikipedia.org/wiki/XLIFF).
See usage: `yarn json2xliff -h`.

The XLIFF 2.0 file will be created in the current working directory with the
provided tag and target language. If the file already exists, the tool throws
an error.

This tool defaults to XLIFF 2.0 . If you need to target version 1.2, you may do
so by adding the flag `--xliff1`, which defaults to false (i.e. XLIFF 2.0).

With the `--stdout` / `-o` flag, you can output to `stdout` and pipe into a
tool like `xq` to format the XML string. A further pipe into `pbcopy` puts the
text into the clipboard, ready to be pasted where you need it.
e.g. `yarn json2xliff .... --stdout | xq | pbcopy`.

```bash
yarn json2xliff --srcLang "de-CH" \
                --trgLang "fr-CH" \
                --srcJson fixtures/uneven_example_de.json \
                --trgJson fixtures/uneven_example_fr.json \
                --tag "v1.3.2" \
                --stdout
```

The output can be sorted and beautified with the flags `--sort` and `--beautify`.

### web app and docker / containers

For convenience purpose where nodejs is not available, the tool is also
packaged as a Docker container.<br>
Before version 1.0.0, the container wraps the command line tool, please read
the relevant README.md file.

Starting with version 1.0.0, a webapp packages the functionality of the command
line tool.<br>
The webapp does not do anything server side: relevant parts of the conversion
logic has been extracted and packaged with Browserify in order to run in the
browser: you can convert your files in your browser directly, without sending
data to the cloud.

Usage: `yarn serve` or `yarn node server.js`.<br>
By default, the app is served on port `3000`: `http://localhost:3000`.

#### Using Docker

It was tested with Podman.<br>

```bash
# pull the image
podman pull registry.gitlab.com/leading-works/floss/json2xliff

podman run --interactive --rm registry.gitlab.com/leading-works/floss/json2xliff:latest
```

Please also note, the example assumes you want to run a transient container and
remove it after use (`--interactive --rm`), you may want to keep it on your
disk with a name (e.g. `--name json2xliff`) and reuse it. This may be useful if
your processes regularly require submitting translations for review, for
example.

## Validating against the XSD schema

For practical reasons and limitations, no validation is provided by this tool.
You may want to validate your XLIFF 2.0 files against [this online validator](https://dev.maxprograms.com/Validation/).

An offline solution may be done using [OpenXLIFF](https://github.com/rmraya/OpenXLIFF).

## License

See LICENSE.
This project is meant to be a free, libre and open source software with the
GPLv3 license.

## Contribute

This project has been done pro bono and open sourced with a GPLv3 license.<br>
We believe that translating apps should be easy and come early in projects.

If this project could help you, please consider donating to further enable open
source development.

- Paypal: https://www.paypal.me/leadingworks ;
- BTC: `1EvujVHygPCcN5AhjSrw6Bdw6ahDkkYTD6` ;
- Ethereum: `0x5d4129b923ee6B9277B57F237084a7C29E15d4Fa` ;
- BNB / BUSD: `bnb1pzsks0g7semc3c2hre4ty2cmzkjey3ccq376aj` ;
- XRP: `rJNmikSwahBgiwBpQTJtco7B3huXBBPxj5` ;
- ADA: `addr1qyru5vlw95n0qcwr8wl53hq3hpwrtqa84p4kxghuk7smxax39sk94h9n9a7r23r7tpesk2ww89hn8uwcxn04ja8qqkaq7643wl` ;
- DogeCoin: `DKSaFcSS7xWSCgf68iqucPjzF49ebcj23G` ;
- MATIC: `0xc2231e3c4885803833677e18c488c984a917d980` ;
- DOT: `13twG4bV4Fg2Ko5xkbvkGGybHokSaLGw1yuRc52n1sirDPdB` ;
- SHIB: `0x5d4129b923ee6b9277b57f237084a7c29e15d4fa` ;
- TRX: `TRM87YC5NSHKPmUAw23vdgN48xvqu5Dqgt` ;
- SOL: `DzJC4mmqt6bshhBUx51psVzQ26beJHtYLhn2WZbKZwit` ;
- LTC: `LMdDkpT1pcoEvTFxnxvAJjhSsb3HcDkya4` ;
- AVAX: `0x4b12b80eda79c5c2bf006abfd7a064dac43c9a29` ;
- ATOM: `cosmos1acqqwls02wfnp3l6hsd3nvdlgqlfgk9tq50q0v` ;
- XMR: `43B8E4zUi91YK8g4i8mUCEGDMDCN6M2emZMXr2xgsBSQAXumpRsrZbXZ18VVwDdK22VtJpeMVphuEZqQWhxiKNCpRyRebjr` ;
- NEAR: `0c738432d5bb2535b489e7e43dee9544bc5aeafb3b32eb8ce5ecb1f23c42e3ce` ;
- HBAR: `1EvujVHygPCcN5AhjSrw6Bdw6ahDkkYTD6` ;
- RUNE: `thor17252s0t9huj8zetlmfepmjthxtegqdlpsmpgk4` ;

