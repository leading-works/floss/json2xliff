const fastify = require('fastify')({ logger: true })
const path = require('path')


fastify.register(require('@fastify/static'), {
  root: path.join(__dirname, 'public')
})

const start = async () => {
  try {
    await fastify.listen({ host: '0.0.0.0', port: 3000 })
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}

fastify.listen(3000, () => {
  console.log(fastify.printRoutes());
})

start()
