#!/usr/bin/env node
'use strict'

/**
 * json2xliff converts json translation files into xliff and back
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const fs = require('node:fs')
const { xliff2json } = require('./src/xliff2json/xliff2json')
const { json2xliff } = require('./src/json2xliff/json2xliff')
const { bail, loggerFactory } = require('./src/utils')

const args = require('yargs')
  .usage(
    `json2xliff  Copyright (C) 2022 Leading Works SàRL
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to redistribute it
    under certain conditions. Type --license to read the license.

    usage:
    $0 [--srcLang bcp_47] [--trgLang bcp47] [--json json_file] [--xliff xlf_file] [--encoding encoding] [--tag version] [--stdout] [--verbose] [--help]`
  )
  .string('srcLang')
  .describe('srcLang', 'the BCP-47 language tag of the source language')
  .string('srcJson')
  .describe(
    'srcJson',
    'the path to the json file with the source texts to convert to xliff 2.0 format, file name must end with .json'
  )
  .string('trgLang')
  .describe('trgLang', 'the BCP-47 lang tag of the target language')
  .string('trgJson')
  .describe(
    'trgJson',
    'an optional path to a json file with existing translations to convert to xliff 2.0 format, file name must end with .json'
  )
  .string('xliff')
  .alias('x', 'xliff')
  .describe(
    'xliff',
    'the path to the xliff file to convert to json format, file must end with .xlf'
  )
  .string('encoding')
  .alias('e', 'encoding')
  .describe('encoding', 'the encoding of the files, defaults to utf-8')
  .string('tag')
  .alias('t', 'tag')
  .describe(
    'tag',
    'the git tag/commit or version of this translation, defaults to random uuid'
  )
  .boolean('stdout')
  .alias('o', 'stdout')
  .describe('stdout', 'outputs the result to stdout')
  .boolean('nodout')
  .alias('n', 'noout')
  .describe(
    'noout',
    'does not try to write the result file, useful with --stdout'
  )
  .boolean('verbose')
  .alias('v', 'verbose')
  .describe('verbose', 'prints verbose debug information')
  .boolean('license')
  .describe('license', 'prints the license file')
  .boolean('xliff1')
  .describe(
    'xliff1',
    'targets XLIFF version 1.2, defaults to false (i.e. 2.0)'
  )
  .boolean('sort')
  .describe('sort', 'sorts the output file\'s keys')
  .boolean('beautify')
  .describe('beautify', 'beautifies the output')
  .skipValidation('license')
  .demand(['srcLang', 'trgLang', 'tag'])
  .alias('h', 'help')
  .help().argv

const {
  encoding,
  noout,
  srcLang,
  srcJson,
  stdout,
  tag,
  trgLang,
  trgJson,
  xliff,
  verbose,
  license,
  xliff1,
  sort,
  beautify
} = args

if (license) {
  const licenseFileDescriptor = fs.openSync('./LICENSE')
  const licenseFileContent = fs.readFileSync(licenseFileDescriptor, 'utf-8')
  console.log(licenseFileContent)
  fs.closeSync(licenseFileDescriptor)
  process.exit(0)
}

const debug = loggerFactory('debug', verbose)

if (srcJson != null && xliff != null) {
  bail(
    3,
    'Both json and xliff files were provided, unclear what needs to be done.\nSee usage with the --help flag.'
  )
}

const cwd = __dirname

if (xliff != null) {
  xliff2json(xliff, trgLang, tag, encoding, stdout, verbose, cwd, noout, sort, beautify)
} else if (srcJson != null) {
  json2xliff(
    srcLang,
    srcJson,
    trgLang,
    trgJson,
    tag,
    encoding,
    stdout,
    verbose,
    cwd,
    noout,
    xliff1,
    sort,
    beautify
  )
} else {
  bail(
    3,
    'Neither xliff file nor srcJson file were provided.\nSee usage with the --help flag.'
  )
}

debug('\nDone.')

process.exit(0)
