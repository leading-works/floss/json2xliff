const { prepareXliffContent } = require('./json2xliff/prepareXliffContent')
const { prepareJsonContent } = require('./xliff2json/prepareJsonContent')

window.json2xliff = window.json2xliff || {}
window.json2xliff.prepareXliffContent = prepareXliffContent
window.json2xliff.prepareJsonContent = prepareJsonContent

module.exports = {
  prepareXliffContent,
  prepareJsonContent
}
