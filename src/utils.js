/**
 * json2xliff converts json translation files into xliff and back
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

exports.loggerFactory = function loggerFactory (logger, enabled) {
  switch (logger) {
    case 'debug':
      return function debug (...msg) {
        if (enabled) {
          console.debug(...msg)
        }
      }
    case 'stdout':
      return function stdout (...msg) {
        if (enabled) {
          console.log(...msg)
        }
      }
    default:
      return function noOp (..._msg) { }
  }
}

exports.bail = function bail (errorCode = 9, msg = 'Exiting now.') {
  console.error(msg)
  process.exit(errorCode)
}

exports.isBlank = function isBlank (t) {
  if (t == null) {
    return true
  } else if (typeof t !== 'string') {
    return true
  } else if (t.trim().length <= 0) {
    return true
  }
  return false
}

/**
 * mergeArrays merges arrays and remove duplicates
 * @param arrays an array of arrays to merge
 * @returns a new array with all the entries of each array and no duplicate
 */
exports.mergeArrays = function mergeArrays (arrays) {
  return Array.from(
    new Set(
      arrays.reduce((whole, part) => whole.concat(part), [])
    )
  )
}

/**
 * identity the identity function, gives back verbatim what it receives
 * @param t any argument
 * @returns the same param t
 */
exports.identity = function identity (t) {
  return t
}

/**
 * middleware offers the possibility to switch between two functions depending
 *            on a flag
 * @param fn1 the function to run when the flag is true
 * @param fn2 the function to run when the flag is false
 * @flag a boolean flag to test
 * @returns the function to use
 * @usage middleware(sort).when(flag).orElse(identity)
 */
exports.middleware = function middleware (fn1) {
  return {
    when: function when (flag) {
      return {
        orElse: function orElse (fn2) {
          return flag ? fn1 : fn2
        }
      }
    }
  }
}

/**
 * beautifyJson beautifies a JSON object
 * @param json the json object to beautify
 * @returns the beautified json object
 */
exports.beautifyJson = function beautifyJson (json) {
  return JSON.stringify(json, null, 2)
}

/**
 * minifyJson stringifies a JSON object in standard way
 * @param json the json object to minify
 * @returns the miniified json object
 */
exports.minifyJson = function minifyJson (json) {
  return JSON.stringify(json)
}

/**
 * sortJson sorts a json object by keys alphabetically
 * @params json a json object
 * @returns the same json object, sorted by key alphabetically
 */
exports.sortJson = function sortJson (json) {
  const keys = Object.keys(json).sort()
  const sorted = {}
  keys.forEach(k => sorted[k] = json[k])
  return sorted
}

