/**
 * json2xliff converts json translation files into xliff and back
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const fs = require('node:fs')
const path = require('path')
const { bail, loggerFactory } = require('../utils')
const { prepareJsonContent } = require('./prepareJsonContent')

/**
 * xliff2json converts an xliff file into a json file
 *            it assumes a flat structure
 *            fails if the file already exists
 * @param xliff the path to the xliff file
 * @param trgLang the BCP-47 language tag of the target language translation
 * @param tag the git commit or tag, or a version of the file to translate
 * @param encoding the encoding of xliff file, defaults to utf-8
 * @param stdout a boolean flag to print the result file to stdout
 * @param verbose a boolean flag to log more debug data
 * @param outDir the path where to save the output file
 * @param noout a boolean flag to prevent writing the result file
 * @param sort a boolean flag to sort the keys in the output file, defaults to false
 * @param beautify a boolean flag to beautify the output 
 */
exports.xliff2json = function xliff2json (
  xliff,
  trgLang,
  tag,
  encoding,
  stdout,
  verbose,
  outDir,
  noout,
  sort = false,
  beautify = false
) {
  const debug = loggerFactory('debug', verbose)
  const toStdout = loggerFactory('stdout', stdout)
  const fileEncoding = encoding || 'utf-8'
  const filename = path.basename(xliff, '.xlf')

  const xliffFileDescriptor = fs.openSync(xliff)
  const xliffFileContent = fs.readFileSync(xliffFileDescriptor, fileEncoding)
  if (xliffFileContent == null) {
    fs.closeSync(xliffFileDescriptor)
    bail(1, `File content for file ${xliff} was empty.`)
  }

  const jsonContent = prepareJsonContent(xliffFileContent, verbose, sort, beautify)
  toStdout(jsonContent)

  if (!noout) {
    const jsonOutFilename = `${outDir}/${filename}_${tag}_${trgLang}.json`
    debug(`Write json file to ${jsonOutFilename}`)
    const jsonOut = fs.openSync(jsonOutFilename, 'ax')
    fs.writeSync(jsonOut, jsonContent)
    fs.closeSync(jsonOut)
  }

  fs.closeSync(xliffFileDescriptor)
  debug('Closed file descriptors.')
}
