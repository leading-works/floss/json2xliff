/**
 * json2xliff converts json translation files into xliff and back
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { compose, lensPath, lensProp, mergeAll, view } = require('ramda')
const { beautifyJson, identity, isBlank, middleware, minifyJson, sortJson } = require('../utils')

exports.JsonAdapter1 = function JsonAdapter1 (xmlContent, sort = false, beautify = false) {
  const units = lensPath(['xliff', 'file', 'body', 'trans-unit'])
  const id = lensProp('@_id')
  const target = lensProp('target')

  function toJsonDirection (direction) {
    return function toJson (segment) {
      return {
        [view(id, segment)]: view(direction, segment)
      }
    }
  }

  const toJsonTarget = toJsonDirection(target)
  const segments = view(units, xmlContent)
  const targetJson = mergeAll(segments.map(toJsonTarget))

  const flatJson = {}
  for (const [k, v] of Object.entries(targetJson)) {
    const oldValue = typeof v === 'object' ? v['#text'] : v
    const newValue = isBlank(oldValue) ? '' : oldValue
    flatJson[k] = newValue
  }

  const doSort = middleware(sortJson).when(sort).orElse(identity)
  const doBeautify = middleware(beautifyJson).when(beautify).orElse(minifyJson)
  return compose(doBeautify, doSort)(flatJson)
}
