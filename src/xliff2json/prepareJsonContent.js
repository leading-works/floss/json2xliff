/**
 * json2xliff converts json translation files into xliff and back
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { XMLParser } = require('fast-xml-parser')
const { loggerFactory } = require('../utils')
const { JsonAdapter1 } = require('./jsonAdapter_v1.2')
const { JsonAdapter2 } = require('./jsonAdapter_v2.0')

/**
 * prepareJsonContent converts an xliff structure into a json file
 *                    it assumes a flat structure
 * @param xliffFileContent the xliff file content
 * @param verbose a boolean flag to log more debug data
 * @param sort a boolean flag to sort the keys in the output file, defaults to false
 * @param beautify a boolean flag to beautify the output 
 */
exports.prepareJsonContent = function prepareJsonContent (
  xliffFileContent,
  verbose,
  sort = false,
  beautify = false
) {
  const debug = loggerFactory('debug', verbose)

  const options = {
    ignoreAttributes: false,
    attributeNamePrefix: '@_'
  }
  const xmlParser = new XMLParser(options)
  const xmlContent = xmlParser.parse(xliffFileContent)
  debug(JSON.stringify(xmlContent))

  const xliffVersion = xmlContent.xliff['@_version']
  const JsonAdapter = xliffVersion === '2.0' ? JsonAdapter2 : JsonAdapter1
  debug(xliffVersion)

  const jsonContent = JsonAdapter(xmlContent, sort, beautify)
  debug(jsonContent)

  return jsonContent
}
