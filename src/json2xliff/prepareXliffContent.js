/**
 * json2xliff converts json translation files into xliff and back
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const xml = require('xml')
const { v4: uuidv4 } = require('uuid')
const { isBlank, loggerFactory } = require('../utils')
const { XliffObjectV1 } = require('./XliffObject_v1.2')
const { XliffObjectV2 } = require('./XliffObject_v2.0')

/**
 * prepareXliffContent converts a json flat translation file into a xliff file
 *                     it assumes a flat structure
 * @param srcLang a BCP-47 language tag of the source language, required
 * @param srcJsonFileContent the source texts file content
 * @param trgLang a BCP-47 language tag of the target language, required
 * @param trgJsonFileContent the existing translations file content
 * @param tag the git commit or tag, or a version of the file to translate
 * @param fileId a string of the id of the file
 * @param verbose a boolean flag to log more debug data
 * @param xliff1 a boolean flag to target XLIFF 1.2 version, defaults to false
 * @returns the xliff content
 */
exports.prepareXliffContent = function prepareXliffContent (
  srcLang,
  srcJsonFileContent,
  trgLang,
  trgJsonFileContent,
  tag,
  fileId,
  verbose,
  xliff1 = false,
  sort = false,
  beautify = false
) {
  const debug = loggerFactory('debug', verbose)
  const XliffObject = xliff1 ? XliffObjectV1 : XliffObjectV2
  const xliffFileVersion = tag == null || isBlank(tag) ? uuidv4() : tag
  const xmlOptions = { indent: beautify ? '  ' : undefined }

  const jsonInput = new XliffObject(
    xliffFileVersion,
    srcLang,
    trgLang,
    fileId,
    sort
  )
  jsonInput.fromJson(
    JSON.parse(srcJsonFileContent),
    JSON.parse(trgJsonFileContent)
  )
  debug(JSON.stringify(jsonInput))

  const xliffContent = xml(jsonInput.toJsonDto(), xmlOptions)
  debug(xliffContent)

  return xliffContent
}
