/**
 * json2xliff converts json translation files into xliff and back
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { mergeArrays } = require('../utils')

/**
 * see the XLIFF 2.0 specification under specification/xliff_core_2.0.xsd
 * required minimal structure is:
 * <xliff
 *   version=""
 *   srcLang=""       <--- BCP-47 tag
 *   trgLang="">      <--- optional in spec, but required here
 *   <file            <--- one or more
 *     id="">         <--- type of NMTOKEN
 *     <unit          <--- either unit or group (of units), at least one
 *       id="">       <--- required, type is NMTOKEN
 *       <segment>    <--- one or more
 *         <source    <--- one
 *           lang=""> <--- optional BCP-47 tag
 *         </source>
 *         <target    <--- optional, but required here, one
 *           lang=""> <--- optional BCP-47 tag
 *         </target>
 *     </unit>
 *   </file>
 * </xliff>
 */
function XliffObjectV2 (version, srcLang, trgLang, fileId, sort = false) {
  this.version = version
  this.srcLang = srcLang
  this.trgLang = trgLang
  this.fileId = fileId
  this.units = []
  this.sort = sort || false
}
XliffObjectV2.prototype.fromJson = function fromJson (src, trg) {
  const srcKeys = Object.keys(src)
  const trgKeys = trg == null ? [] : Object.keys(trg)
  const allKeys = this.sort
    ? mergeArrays([srcKeys, trgKeys]).sort()
    : mergeArrays([srcKeys, trgKeys])

  // parse each entry, assuming o is flat
  allKeys.forEach((k) => {
    this.units.push({
      id: k,
      segment: {
        source: src[k] || '',
        target: trg == null ? '' : trg[k] || ''
      }
    })
  })
}
XliffObjectV2.prototype.toJsonDto = function toJsonDto () {
  return [
    {
      xliff: [
        {
          _attr: {
            xmlns: 'urn:oasis:names:tc:xliff:document:2.0',
            version: '2.0',
            srcLang: this.srcLang,
            trgLang: this.trgLang
          }
        },
        {
          file: [
            {
              _attr: {
                id: this.version,
                original: this.fileId
              }
            },
            ...this.units.map((unit) => ({
              unit: [
                {
                  _attr: {
                    id: unit.id
                  }
                },
                {
                  segment: [
                    {
                      source: [unit.segment.source]
                    },
                    {
                      target: [unit.segment.target]
                    }
                  ]
                }
              ]
            }))
          ]
        }
      ]
    }
  ]
}

exports.XliffObjectV2 = XliffObjectV2
