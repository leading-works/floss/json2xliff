/**
 * json2xliff converts json translation files into xliff and back
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { mergeArrays } = require('../utils')

/**
 * see the XLIFF 1.2 specification under specification/xliff_core_1.2.xsd
 * required minimal structure is:
 * <xliff
 *   version="1.2">
 *   <file                    <--- one or more
 *     id=""                  <--- type of NMTOKEN
 *     datatype="plaintext"   <--- required, in our case, always plaintext
 *     source-language=""     <--- BCP-47 tag
 *     target-language="">    <--- optional in spec, but required here
 *     <body>
 *       <trans-unit          <--- either unit or group (of units), at least one
 *         id="">             <--- required, type is NMTOKEN
 *           <source />       <--- one
 *           <target />       <--- optional, but required here, one
 *       </trans-unit>
 *     </body>
 *   </file>
 * </xliff>
 */
function XliffObjectV1 (version, srcLang, trgLang, fileId, sort = false) {
  this.version = version
  this.srcLang = srcLang
  this.trgLang = trgLang
  this.fileId = fileId
  this.units = []
  this.sort = sort || false
}
XliffObjectV1.prototype.fromJson = function fromJson (src, trg) {
  const srcKeys = Object.keys(src)
  const trgKeys = trg == null ? [] : Object.keys(trg)
  const allKeys = this.sort
    ? mergeArrays([srcKeys, trgKeys]).sort()
    : mergeArrays([srcKeys, trgKeys])

  // parse each entry, assuming o is flat
  allKeys.forEach((k) => {
    this.units.push({
      id: k,
      source: src[k] || '',
      target: trg == null ? '' : trg[k] || ''
    })
  })
}
XliffObjectV1.prototype.toJsonDto = function toJsonDto () {
  return [
    {
      xliff: [
        {
          _attr: {
            xmlns: 'urn:oasis:names:tc:xliff:document:1.2',
            version: '1.2'
          }
        },
        {
          file: [
            {
              _attr: {
                'original': this.fileId,
                'datatype': 'plaintext',
                'source-language': this.srcLang,
                'target-language': this.trgLang
              }
            },
            {
              body: [
                ...this.units.map((unit) => ({
                  'trans-unit': [
                    {
                      _attr: {
                        id: unit.id
                      }
                    },
                    {
                      source: unit.source
                    },
                    {
                      target: unit.target
                    }
                  ]
                }))
              ]
            }
          ]
        }
      ]
    }
  ]
}

exports.XliffObjectV1 = XliffObjectV1
