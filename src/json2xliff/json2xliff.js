/**
 * json2xliff converts json translation files into xliff and back
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const fs = require('node:fs')
const path = require('path')
const { bail, loggerFactory } = require('../utils')
const { prepareXliffContent } = require('./prepareXliffContent')

/**
 * json2xliff converts a json flat translation file into a xliff 2.0 file
 *            it assumes a flat structure
 *           fails if the file already exists
 * @param srcLang a BCP-47 language tag of the source language, required
 * @param srcJson the path to the json file with the source translations
 * @param trgLang a BCP-47 language tag of the target language, required
 * @param trgJson the path to the json file with the existing target translations, optional (use null)
 * @param tag the git commit or tag, or a version of the file to translate
 * @param encoding the encoding of xliff file, defaults to utf-8
 * @param stdout a boolean flag to print the result file to stdout
 * @param verbose a boolean flag to log more debug data
 * @param outDir the path where to save the output file
 * @param noout a boolean flag to prevent writing the result file
 * @param xliff1 a boolean flag to target XLIFF 1.2 version, defaults to false
 * @param sort a boolean flag to sort the keys in the output file, defaults to false
 * @param beautify a boolean flag to beautify the output 
 */
exports.json2xliff = function json2xliff (
  srcLang,
  srcJson,
  trgLang,
  trgJson,
  tag,
  encoding,
  stdout,
  verbose,
  outDir,
  noout,
  xliff1 = false,
  sort = false,
  beautify = false
) {
  const debug = loggerFactory('debug', verbose)
  const toStdout = loggerFactory('stdout', stdout)

  const fileEncoding = encoding || 'utf-8'
  const filename = path.basename(srcJson, '.json')
  const fileId = `${filename}.json`

  const srcJsonFileDescriptor = fs.openSync(srcJson)
  const srcJsonFileContent = fs.readFileSync(
    srcJsonFileDescriptor,
    fileEncoding
  )
  if (srcJsonFileContent == null) {
    fs.closeSync(srcJsonFileDescriptor)
    bail(1, `File content for file ${srcJson} was empty.`)
  }

  let trgJsonFileDescriptor
  let trgJsonFileContent = null
  if (trgJson != null) {
    trgJsonFileDescriptor = fs.openSync(trgJson)
    trgJsonFileContent = fs.readFileSync(trgJsonFileDescriptor, fileEncoding)
  }

  const xliffContent = prepareXliffContent(
    srcLang,
    srcJsonFileContent,
    trgLang,
    trgJsonFileContent,
    tag,
    fileId,
    verbose,
    xliff1,
    sort,
    beautify
  )
  toStdout(xliffContent)

  if (!noout) {
    const xliffOutFilename = `${outDir}/${filename}_${tag}_${trgLang}.xlf`
    debug(`Write xliff file to ${xliffOutFilename}`)
    const xliffOut = fs.openSync(xliffOutFilename, 'ax')
    fs.writeSync(xliffOut, xliffContent)
    fs.closeSync(xliffOut)
  }

  if (trgJsonFileDescriptor != null) {
    fs.closeSync(trgJsonFileDescriptor)
  }

  fs.closeSync(srcJsonFileDescriptor)
  debug('Closed file descriptors.')
}
