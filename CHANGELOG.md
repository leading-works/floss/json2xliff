# 1.1.0 / 2022-12-03

## Summary

This is a non critical update.

It is backward compatible up until 1.0.0<br>
Previous versions offered the CLI interface in the Docker container. 1.0.0
replaces it with a progressive web app.<br>
The CLI interface is backwards compatible until version 0.2.0 which modified
the command name from `convert` to `json2xliff` for consistency with the Docker
image name.

This release fix a missing attribute on XLIFF files v1.2 (`datatype`) and add
the possibility to sort the keys alphabetically and beautify the outputs. They
are deactivated per default, which yields the same behaviour as the previous
release.

## Changes

* `Fixed` missing `datatype` attribute on the `file` tag of XLIFF files v1.2 ;
* `Fixed` wrong URL in the **Open Source** web app tab ;
* `Added` link to tldrlegal.com's page about GPLv3 in the **Open Source** web app tab ;
 
